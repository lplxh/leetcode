class Solution {
public:
    vector<vector<int>> matrixReshape(vector<vector<int>>& nums, int r, int c) {
        vector<vector<int>> ans(r,vector<int>(c,0));
        if(nums.empty()) return nums;
        int row = nums.size();
        int col = nums[0].size();
        if(row*col != r*c) return nums;
        int indexr= 0,indexc= 0;
        for(int i=0;i<r;i++){
            for(int j=0;j<c;j++){
                ans[i][j]=nums[indexr][indexc++];
                if(indexc==col){indexc=0;indexr++;}
            }
        }
        return ans;
    }
};