class Solution {
public:
    struct nodes{
        int index;
        int score;
        bool operator< (const nodes& other) const{
            if(score==other.score){
                return index<other.index;
            }
            else return score>other.score;
        }
    };
    vector<string> findRelativeRanks(vector<int>& nums) {
        vector<nodes> buf;
        vector<string> ans(nums.size());
        for(int i=0;i<nums.size();i++){
            buf.push_back({i,nums[i]});
        }
        sort(buf.begin(),buf.end());
        for(int i=0;i<buf.size();i++){
            if(i==0) ans[buf[i].index]="Gold Medal";
            else if(i==1) ans[buf[i].index]="Silver Medal";
            else if(i==2) ans[buf[i].index]="Bronze Medal";
            else ans[buf[i].index]=to_string(i+1);
        }
        return ans;
    }
};