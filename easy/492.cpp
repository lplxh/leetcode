class Solution {
public:
    vector<int> constructRectangle(int area) {
        int tmp = sqrt(area);
        vector<int> ans(2,0);
        while (area % tmp != 0) --tmp;
        ans[0]=area/tmp;
        ans[1]=tmp;
        return ans;
    }
};