class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        ListNode*front=head,*pre=NULL;
        while(head!=NULL){
            if(head->val==val){
                if(front==head){
                    front=head->next;
                    pre=head;
                    head=head->next;
                }
                else{
                    head=head->next;
                    pre->next=head;
                }
            }
            else{
                pre=head;
                head=head->next;
            }
        }
        return front;
    }
};