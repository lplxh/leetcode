class Solution {
public:
    void reverseString(vector<char>& s) {
        int num=s.size();
        char tmp;
        for(int i=0;i<num/2;i++){
            tmp=s[i];
            s[i]=s[num-1-i];
            s[num-1-i]=tmp;
        }
    }
};