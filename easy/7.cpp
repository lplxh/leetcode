class Solution {
public:
    int reverse(int x) {
        int remainder,Q;
        long ans=0;
        while(x != 0){
            remainder = x%10;
            Q = x/10;
            x = Q;
            ans = ans*10 + remainder;
            if(ans >INT_MAX || ans < INT_MIN){
                return 0;
            }
        }
        return (int)ans;        
    }
};