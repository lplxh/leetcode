class Solution {
public:
    vector<vector<int>> imageSmoother(vector<vector<int>>& M) {
        int line=M.size(),row=M[0].size();
        vector<vector<int>> ans(line);
        for(int i=0;i<line;i++){
            for(int j=0;j<row;j++){
                int tmp=0;
                int count=0;
                for(int a=i-1;a<=i+1;a++){
                    if(a<0||a>=line) continue;
                    for(int b=j-1;b<=j+1;b++){
                        if(b<0||b>=row) continue;
                        else{
                            tmp+=M[a][b];
                            count++;
                        }
                    }
                }
                ans[i].push_back(tmp/count);
            }
        }
        return ans;
    }
};