class Solution {
public:
    vector<int> findErrorNums(vector<int>& nums) {
        sort(nums.begin(),nums.end());
        int repeat , mis=1;
        for(int i = 0;i<nums.size()-1;i++){
            if(nums[i]==nums[i+1]) repeat = nums[i];
            if(mis == nums[i]) mis++;
        }
        if(mis == nums[nums.size()-1]) mis++;
        return vector<int>{repeat,mis};
    }
};