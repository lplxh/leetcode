class Solution {
public:
    int countSegments(string s) {
        int num=s.size();
        if(s.empty()) return 0;
        int i,n=0;
        bool label=true;
        for(i=0;i<num;i++){
            if(s[i]!=' '&&label==true) {n++; label=false;}
            if(s[i]==' ') label=true;
        }
        return n;
    }
};