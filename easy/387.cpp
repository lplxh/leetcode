class Solution {
public:
    int firstUniqChar(string s) {
        int num=s.size();
        for(int i=0;i<num;i++){
            if(s.find(s[i])==s.rfind(s[i])) return s.find(s[i]);
        }
        return -1;
    }
    
};