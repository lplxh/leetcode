class Solution {
public:
    vector<int> getRow(int rowIndex) {
        int n = rowIndex+1;
        vector<int> ans(n);
        int j=0;
        for(int i=0;i<=n/2;i++){
            if(i==0) ans[i]=ans[n-1-i]=1;
            else{
                ans[i]=ans[n-1-i]=(long long)ans[i-1]*(rowIndex-i+1)/i;
            }
        }
        return ans;
    }
};