class Solution {
public:
    int mySqrt(int x) {
        int l=0,r=x,res;
        long long mid;
        while(l<=r){
            mid=(l+r)/2;
            long long tmp=mid*mid;
            if(tmp>x) r=mid-1;
            else{
                res=mid;
                l=mid+1;
            }
        }
        return res;
    }
};