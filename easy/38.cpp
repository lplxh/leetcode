class Solution {
public:
    string countAndSay(int n) {
        if(n==1) return "1";
        else{
            string analysis = countAndSay(n-1);
            string ans="";
            int count=0;
            for(int i=0;i<analysis.size();i++){
                if(i != analysis.size()-1 && analysis[i]==analysis[i+1]) count++;
                else{
                    ans+=to_string(count+1)+analysis[i];
                    count=0;
                }
            }
            return ans;
        }
    }
};