class Solution {
public:
    int findShortestSubArray(vector<int>& nums) {
        map<int,vector<int>> array;
        int n = nums.size();
        for(int i=0;i<n;i++){
            if(array[nums[i]].size()==0) array[nums[i]]={1,i,i};
            else{
                array[nums[i]][0]++;
                array[nums[i]][2]=i; 
            }
        }
        int num=0,length=0;
        for(map<int,vector<int>>::iterator it =array.begin();it !=array.end();it++){
            if(it->second[0]>num){
                length = it->second[2]-it->second[1]+1;
                num = it->second[0];
            }
            else if(it->second[0]==num){
                if(length>it->second[2]-it->second[1]+1){
                    length = it->second[2] - it->second[1]+1;
                }
            }
        }
        return length;
    }    
};