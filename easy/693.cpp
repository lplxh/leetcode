class Solution {
public:
    bool hasAlternatingBits(int n) {
        if(n==1 || n==0) return true;
        int pre=(n&1);
        int now;
        n=(n>>1);
        while(n){
            now=(n&1);
            if(now==pre) return false;
            pre=now;
            n=(n>>1);
        } 
        return true;
    }
};