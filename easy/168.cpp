class Solution {
public:
    string convertToTitle(int n) {
        string ans;
        int remainer;
        while(n>0){
            remainer=n%26;
            n=n/26;
            if(remainer==0) {
                n--;
                ans="Z"+ans;
            }
            else ans=string(1,'A'+remainer-1)+ans;
        }
        return ans;
    }
};