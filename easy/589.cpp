class Solution {
public:
    vector<int> preorder(Node* root) {
        vector<int> vec;
        pre(root,vec);
        return vec;
    }
    vector<int> pre(Node* root,vector<int>& vec){
        if(root==nullptr) return vec;
        vec.push_back(root->val);
        for(int i=0;i<root->children.size();i++){
            pre(root->children[i],vec);
        }
        return vec;
    }
};