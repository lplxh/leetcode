class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        int maxsize=1;
        vector<vector<int>> ans(numRows);
        for(int i=0;i<numRows;i++){
            for(int j=0;j<maxsize;j++){
                if(j==0 || j==maxsize-1) ans[i].push_back(1);
                else ans[i].push_back(ans[i-1][j-1]+ans[i-1][j]);
            }
            maxsize++;
        }
        return ans;
    }
};