class Solution {
public:
    int thirdMax(vector<int>& nums) {
        long i=LONG_MIN,j=LONG_MIN,k= LONG_MIN;
        int n = nums.size();
        for(int a=0;a<n;a++){
            long  tmp1 = min((long)nums[a],i);
            i = max((long)nums[a],i);
            if(tmp1 == i) continue;
            long tmp2 = min((long)tmp1,j);
            j = max(tmp1,j);
            if(tmp2 == j) continue;
            long tmp3 = min((long)tmp2,k);
            k = max(tmp2,k);
        }
        if(k == LONG_MIN) return (int)i;
        else return (int)k;
    }
};