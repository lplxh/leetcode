class NumArray {
private:
    int*sums;
public:
    NumArray(vector<int>& nums) {
        sums=new int[nums.size()];
        int tmp=0;
        for(int i=0;i<nums.size();i++){
            tmp+=nums[i];
            sums[i]=tmp;
        }
    }
    ~NumArray(){
        delete[] sums;
    }
    int sumRange(int i, int j) {
        if(i==0) return sums[j];
        else return sums[j]-sums[i-1];
    }
};