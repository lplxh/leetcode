class Solution {
public:
    bool isSubsequence(string s, string t) {
        int nums=s.size();
        int numt=t.size();
        int i=0,j=0;
        while(i<nums && j<numt){
            if(s[i]==t[j]) i++;
            j++;
        }
        if(i==nums) return true;
        else return false;
    }
};