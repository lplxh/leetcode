class Solution {
public:
    bool checkPossibility(vector<int>& nums) {
        int n = nums.size();
        bool label =true;
        for(int i=0;i<n-1;){
            if(nums[i+1]<nums[i]){
                if(label == false) return false;
                if(i+2<=n-1 && nums[i+2]<nums[i+1]) return false;
                if(i+2>n-1) return true;
                else if(nums[i+2]>=nums[i]){
                    nums[i+1] = nums[i+2];
                    i = i+2;
                    label = false;
                }
                else if(nums[i+2]<nums[i]){
                    if(i == 0 || nums[i+1] >= nums[i-1]){
                        nums[i] = nums[i+1];
                        i = i+2;
                        label = false;
                    }
                    else if(nums[i+1]<nums[i-1]){
                        return false;
                    }

                    
                }
            }
            else i++;
        }
        return true;
    }
};