class Solution {
public:
    bool canConstruct(string ransomNote, string magazine) {
        map<char,int>cons;
        for(int i=0;i<magazine.size();i++){
            if(cons.find(magazine[i])==cons.end()) cons[magazine[i]]=1;
            else{
                cons[magazine[i]]++;
            }
        }
        for(int j=0;j<ransomNote.size();j++){
            if(cons.find(ransomNote[j])==cons.end()) return false;
            else{
                cons[ransomNote[j]]--;
                if(cons[ransomNote[j]]<0) return false;
            }
        }
    //    for(map<char,int>::iterator item=cons.begin();item!=cons.end();item++){
    //        if(item->second>0) return false;
    //    }
        return true;
    }
};