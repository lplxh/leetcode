class Solution {
public:
    int calPoints(vector<string>& ops) {
        vector<int> mark;
        int tmp;
        for(int i=0;i<ops.size();i++){
            if(ops[i]=="+"){
                tmp=mark[mark.size()-1]+mark[mark.size()-2];
                mark.push_back(tmp);
            }
            else if(ops[i]=="D"){
                tmp=mark[mark.size()-1]*2;
                mark.push_back(tmp);
            }
            else if(ops[i]=="C"){
                mark.pop_back();
            }
            else{
                mark.push_back(stoi(ops[i]));
            }
        }
        int ans=0;
        for(int i=0;i<mark.size();i++){
            ans+=mark[i];
        }
        return ans;
    }
};