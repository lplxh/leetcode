class Solution {
public:
    bool detectCapitalUse(string word) {
        bool label = false;
        bool tmp = true;
        if(word.size()==1) return true; 
        for(int i=1;i<=word.size()-1;i++){
            if(judge(word[i])>judge(word[i-1])){
                return false;
            }
            if(judge(word[0])==judge(word[1]) && judge(word[i])<judge(word[i-1])){
                return false;
            }
        }
        return true;
    }
    int judge(char c){
        if(c>='A' && c<='Z') return 1;
        else return 0;
    }
};