class Solution {
public:
    bool isValid(string s) {
        int num=s.size();
        stack<char> t;
        if(num==0) return true;
        else if(s[0]=='(' ||s[0]=='{' || s[0]=='[')
            t.push(s[0]);
        else return false;
        for(int i=1;i<num;i++){
            if(s[i]=='(' || s[i]=='{' || s[i]=='['){
                t.push(s[i]);
            }
            else{
                if (t.empty()) return false;
                char tmp=t.top();
                t.pop();
                if(tmp=='(' && s[i]==')') continue;
                else if(tmp=='{' && s[i]=='}') continue;
                else if(tmp=='[' && s[i]==']') continue;
                else return false;
            }
        }
        if(!t.empty()){
            return false;
        }
        else return true;
    }
};