class Solution {
public:
    int countBinarySubstrings(string s) {
        vector<int> buf={0};
        vector<int> one;
        one.push_back(0);
//        else one.push_back(-1);
        int num = s.size();
        int ans=0;
        for(int i=1;i<num;i++){
            buf.push_back((int)s[i]-(int)s[i-1]);
        }
        for(int i=0;i<num;i++){
            if(buf[i]==1 || buf[i]==-1) one.push_back(i);
        }
        one.push_back(num);
        for(int i=1;i<one.size()-1;i++){
            ans+=min(one[i]-one[i-1],one[i+1]-one[i]);
        }
        return ans;
    }
};