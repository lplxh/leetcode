class Solution {
public:
    int findComplement(int num) {
        int tmp=log2(num)+1;
        return (unsigned) (2^(tmp-1))-1-num;
    }
};