class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        ListNode*head=nullptr,*top=nullptr;
        while(l1!=nullptr && l2!=nullptr){
            if(l1->val<=l2->val){
                if(head==nullptr) {top=l1;head=l1;}
                else {
                    head->next=l1;
                    head=head->next;
                }
                l1=l1->next;
            }
            else{
                if(head==nullptr) {top=l2;head=l2;}
                else {
                    head->next=l2;
                    head=head->next;
                }
                l2=l2->next;
            }
        }
        if(l1!=nullptr) {
            if(head==nullptr){
                top=l1;
                head=l1;
            }
            else{head->next=l1;}
        }
        else if(l2!=nullptr) {
            if(head==nullptr){
                top=l2;
                head=l2;
            }
            else{head->next=l2;}
        }
        return top;
    }
};