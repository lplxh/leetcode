class Solution {
public:
    bool isPowerOfThree(int n) {
        if(n<=0) return false;
        else{
            double ans = log10(n)/log10(3);
            if(ans!=(int)ans){
                return false;
            } 
            else return true;
        }
    }
};