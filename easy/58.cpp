class Solution {
public:
    int lengthOfLastWord(string s) {
        int num=s.size();
        int n=0;
        bool label=false;
        for(int i=0;i<num;i++){
            if(label==false&&s[i]!=' '){
                n++;
            }
            else if(s[i]==' '){
                label=true;
            }
            else if(label==true&&s[i]!=' '){
                n=1;
                label=false;
            }
        }
        return n;
    }
};