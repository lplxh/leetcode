class Solution {
public:
    string reverseWords(string s) {
        int num=s.size();
        int start=0,end=0;
        for(int i=0;i<=num;i++){
            if(s[i]==' '||s[i]=='\0'){
                end=i;
                for(int k=start,j=end-1;k<j;k++,j--){
                    swap(s[k],s[j]);
                }
                start=end+1;
            }

        }
        return s;
    }
};