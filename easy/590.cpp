class Solution {
public:
     vector<int> postorder(Node* root) {
        vector<int> vec;
        post(root,vec);
        return vec;
    }
    vector<int> post(Node* root,vector<int>& vec){
        if(root==nullptr) return vec;
        for(int i=0;i<root->children.size();i++){
            post(root->children[i],vec);
        }
        vec.push_back(root->val);
        return vec;
    }
};