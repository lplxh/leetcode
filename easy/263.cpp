class Solution {
public:
    bool isUgly(int num) {
        if(num==0) return false; 
        int flag=true;
        while(flag==true && num!=1){
            flag=false;
            if(num%2==0){
                flag=true;
                num=num/2;
            }
            else if(num%3==0){
                flag=true;
                num=num/3;
            }
            else if(num%5==0){
                flag=true;
                num=num/5;
            }
        }
        return flag;
    }
};