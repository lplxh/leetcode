class Solution {
public:
    bool judgeCircle(string moves) {
        int num = moves.size();
        int right=0,up=0;
        for(int i=0;i<num;i++){
            if(moves[i]=='U') up++;
            if(moves[i]=='D') up--;
            if(moves[i]=='R') right++;
            if(moves[i]=='L') right--;
        }
        if(right!=0 || up!=0) return false;
        else return true;
    }
};