class Solution {
public:
    string convertToBase7(int num) {
        bool label=true;
        string ans;
        if(num==0) return "0";
        if (num < 0){
            num=-num;
            label=false;
        }
            int tmp;
            while(num!=0){
                tmp=num%7;
                ans=to_string(tmp)+ans;
                num=num/7;
            }
        
        if(label==false) ans='-'+ans;
        return ans;
    }
};