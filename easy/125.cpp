class Solution {
public:
    bool isPalindrome(string s) {
        transform(s.begin(),s.end(),s.begin(),::tolower);
        int num = s.size();
        int i=0,j=num-1;
        while(1){
            while(i<=j&&!isalnum(s[i])) i++;
            while(i<=j&&!isalnum(s[j])) j--;
            if(i>j) break;
            if(s[i]!=s[j]) return false;
            i++; j--;
        }
        return true;
    }
};