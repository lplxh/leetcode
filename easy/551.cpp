class Solution {
public:
    bool checkRecord(string s) {
        int num=s.size();
        int a=1,l=2;
        for(int i=0;i<num;i++){
            if(s[i]=='A'){
                l=2;
                a--;
            }
            else if(s[i]=='L') l--;
            else l=2;
            if(a<0 || l<0) return false;
        }
        return true;
    }
};