class Solution {
public:
    int maxCount(int m, int n, vector<vector<int>>& ops) {
        int minline = m;
        int minrow = n;
        int num = ops.size();
        for(int i=0;i<num;i++){
            minline = min(minline,ops[i][0]);
            minrow = min(minrow,ops[i][1]);
        }
        return minline*minrow;
    };
};