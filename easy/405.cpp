class Solution {
public:
    string toHex(int num) {
        if(num==0) return "0";
        unsigned int unum = num;
        string ans;
        string buf_16="0123456789abcdef";
        while(unum!=0){
            int tmp = unum&15;
            ans=buf_16[tmp]+ans;
            unum>>=4;
        }
        return ans;
    }
};