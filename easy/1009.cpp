class Solution {
public:
    int bitwiseComplement(int N) {
        if(N==0) return 1;
        int tmp=log2(N)+1;
        return (unsigned) pow(2,tmp)-1-N;
    }
};