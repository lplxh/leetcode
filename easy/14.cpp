class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        int num = strs.size();
        if(num == 0) return "";
        string ans = strs[0];
        if(num == 1) return ans; 
        for(int i=1;i<num;i++){
            ans = comparecommonPrefix(ans,strs[i]);
            if(ans.size() == 0) return "";
        }
        return ans;
    }
    string comparecommonPrefix(string str1,string str2){
        int i;
        int num = min(str1.size(),str2.size());
        for(i = 0;i<num;i++){
            if(str1[i] != str2[i]) break;
        }
        return str1.substr(0,i);
    }
};