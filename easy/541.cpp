class Solution {
public:
    string reverseStr(string s, int k) {
        int num=s.size();
        int start=0,end=-1;
        while(num-end-1>=2*k){ 
            end=start+2*k-1;  
            for(int i=start,j=end-k;i<j;i++,j--){
                swap(s[i],s[j]);
            }
            start+=2*k;
        }
        if(num-end-1<k){
            for(int i=end+1,j=num-1;i<j;i++,j--){
                swap(s[i],s[j]);
            }
        }
        if(num-end-1>=k && num-end-1<2*k){
            for(int i=end+1,j=end+k;i<j;i++,j--){
                swap(s[i],s[j]);
            }
        }
        return s;
    }
};