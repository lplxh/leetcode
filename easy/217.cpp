class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        int n=nums.size();
        unordered_map<int,int> buf;
        for(int i=0;i<n;i++){
            buf[nums[i]]++;
            if(buf[nums[i]]>1) return true;
        }
        return false;
    }
};