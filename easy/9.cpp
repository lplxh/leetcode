class Solution {
public:
    bool isPalindrome(int x) {
        if(x<0) {
            return false;
        }
        else if(x == 0){
            return true;
        }
        else if(x%10==0 && x!=0){
            return false;
        }
        else{
            int ans=0;
            while(ans < x){
                int x1 = x%10;
                int x2 = x/10;
                x = x2;
                ans = ans*10+x1;
            }
            return ans == x || x == ans/10 ;      
        }
    }
};