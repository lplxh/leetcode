class Solution {
public:
    double myPow(double x, int n) {
        if(x==0) return 0;
        double ans;
        if(x>0 || n%2==0) ans=exp(n*log(abs(x)));
        else if(x<0 && n%2==1) ans=-exp(n*log(-x)); 
        return ans;
    }
};