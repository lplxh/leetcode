class Solution {
public:
    int totalHammingDistance(vector<int>& nums) {
        int num = nums.size();
        int ans=0;
        for(int i=0;i<32;i++){
            int one=0,zero=0;
            for(int j=0;j<num;j++){
                if((nums[j] & 1)==1){
                    one++;
                }
                else zero++;
                nums[j]=nums[j]>>1;
            }
            ans+=one*zero;
        }
        return ans;
    }
};