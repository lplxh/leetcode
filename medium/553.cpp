class Solution {
public:
    string optimalDivision(vector<int>& nums) {
        int n=nums.size();
        string ans="";
        if(n==1) return to_string(nums[0]);
        else if(n==2) return to_string(nums[0])+"/"+to_string(nums[1]);
        else{
            for(int i=0;i<n;i++){
                if(i==0) ans+=to_string(nums[i])+"/(";
                else if(i==n-1) ans+=to_string(nums[i])+")";
                else{
                    ans+=to_string(nums[i])+"/";
                }
            }
        }      
        return ans;
    }
};