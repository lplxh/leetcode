class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int n=nums.size();
        for(int start=0,count=0;count<n;start++){
            int index = start;
            int tmp1 = nums[start];
            do{
                index = (index + k) % n;
                int tmp2=nums[index];
                nums[index]=tmp1;
                tmp1=tmp2;
                count++;
            }while(index != start);
        }
    }
};