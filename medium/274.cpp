class Solution {
public:
    int hIndex(vector<int>& citations) {
        sort(citations.begin(),citations.end(),greater<int>());
        int n = citations.size();
        int h = 0;
        for(h=0;h<n;h++){
            if(citations[h]<h+1) break; 
        }
        return h;
    }
};