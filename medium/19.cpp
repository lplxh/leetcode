class Solution {
public:
    int count=0;
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        if(head==nullptr) return nullptr;
        head->next=removeNthFromEnd(head->next,n);
        count++;
        if(count==n) return head->next;
        else return head;
    }
};