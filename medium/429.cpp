class Solution {
public:
    vector<vector<int>> levelOrder(Node* root) {
        vector<vector<int>> vec;
        levelor(root,vec,0);
        return vec;   
    }
    vector<vector<int>> levelor(Node* root,vector<vector<int>>& vec,int level){
        if(root==nullptr) return vec;
        if(vec.size()<=level) vec.resize(level+1);
        vec[level].push_back(root->val);
        for(int i=0;i<root->children.size();i++){
            levelor(root->children[i],vec,level+1);
        }
        return vec;
    }
};