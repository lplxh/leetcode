class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> buf;
        postorder(root,buf);
        return buf;
    }
    void postorder(TreeNode* root,vector<int>&buf){
        if(root==nullptr) return;
        postorder(root->left,buf);
        postorder(root->right,buf);
        buf.push_back(root->val);
    }
};