class Solution {
public:
    int countNumbersWithUniqueDigits(int n) {
        if(n==0) return 1;
        if(n>10) return countNumbersWithUniqueDigits(10);
        if(n==1) return 10;
        if(n==2) return 9*9+10;
        else{
            int ans=9,base=9;
            for(int i=1;i<n;i++){
                ans*=base;
                base--;
            }
            return ans+countNumbersWithUniqueDigits(n-1);
        } 
    }
};