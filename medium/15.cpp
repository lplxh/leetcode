class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        int n = nums.size();
        sort(nums.begin(), nums.end());
        vector<vector<int>> ans;
        for(int first=0;first<n;first++){
            if(first>0 && nums[first]==nums[first-1]){
                continue;
            }
            int third = n-1;
            for(int second=first+1;second<n;second++){
                if(second>first+1 &&nums[second]==nums[second-1]){
                    continue;
                }
                while(nums[first]+nums[second]+nums[third]>0 && second<third){
                    third--;
                }
                if(second==third){
                    break;
                }
                if(nums[first]+nums[second]+nums[third]==0){
                    ans.push_back(vector<int>{nums[first],nums[second],nums[third]});
                }
            }
        } 
        return ans;
    }
};