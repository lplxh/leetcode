class Solution {
public:
    int countBattleships(vector<vector<char>>& board) {
        int line=board.size();
        int row=board[0].size();
        int ans=0;
        for(int i=0;i<line;i++){
            for(int j=0;j<row;j++){
                if(board[i][j]=='X' && (i==0 || board[i-1][j]=='.') && (j==0 || board[i][j-1]=='.')) ans++;
            }
        }
        return ans;
    }
};