class Solution {
public:
    string complexNumberMultiply(string a, string b) {
        
        int signa=0;
        while(a[signa]!='+') signa++;
        int signb=0;
        while(b[signb]!='+') signb++;
        int a1=stoi(a.substr(0,signa));
        int b1=stoi(b.substr(0,signb));
        int a2=stoi(a.substr(signa+1,a.size()-1-signa));
        int b2=stoi(b.substr(signb+1,b.size()-1-signb));
        int re1=a1*b1-a2*b2;
        int re2=a1*b2+a2*b1;
        return to_string(re1)+'+'+to_string(re2)+'i';
    }
};