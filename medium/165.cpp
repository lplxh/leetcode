class Solution {
public:
    int compareVersion(string version1, string version2) {
        int start1=0,end1=0,start2=0,end2=0,num1,num2;
        while(end1<version1.size() || end2<version2.size()){
            if(end1>version1.size()) num1=0;
            else{
                 while(end1<version1.size() && version1[end1] != '.') end1++;
                 num1 = stoi(version1.substr(start1,end1-start1));
                 start1=end1+1;
                 end1=start1;
            }
            if(end2>version2.size()) num2=0;
            else{
                 while(end2<version2.size() && version2[end2] != '.') end2++;
                 num2 = stoi(version2.substr(start2,end2-start2));
                 start2=end2+1;
                 end2=start2;
            }
            if(num1<num2) return -1;
            if(num1>num2) return 1;
        }
        return 0;
    }
};