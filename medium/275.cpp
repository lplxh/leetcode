class Solution {
public:
    int hIndex(vector<int>& citations) {
        int nums=citations.size();
        if(nums==0) return 0;
        int l=0,r=nums;
        while(l<r){
            int mid=l+(r-l+1)/2;
            if(citations[nums-mid]>=mid) l=mid;
            else if(citations[nums-mid]<mid) r=mid-1;
        }
        return r;
    }
};
