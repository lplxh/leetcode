class Solution {
public:
    int maxRotateFunction(vector<int>& A) {
        long num = A.size();
        long  fk=0,sum=0;
        for(int i=0;i<num;i++){
            fk+=i*A[i];
            sum+=A[i];
        }
        long  ans=fk;
        for(int i=1;i<num;i++){
            long  fk_1=fk+sum-num*A[num-i];
            ans =max(ans,fk_1);
            fk = fk_1;

        }
        return ans;
    }