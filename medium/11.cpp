class Solution {
public:
    int maxArea(vector<int>& height) {
        int n = height.size();
        int i = 0;
        int j = n-1;
        int ans = 0;
        while(i<j){
            int tmp = min(height[i],height[j]);
            ans = max(ans,(j-i)*tmp);
            if(height[i]<height[j]){
                i = i+1;
            }
            else j = j-1;
        }
        return ans;
    }
};