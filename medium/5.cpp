class Solution {
public:
    string longestPalindrome(string s) {
        int n = s.size();
        vector<vector<int>> da(n, vector<int>(n));
        string ans;
        for(int l = 0;l < n;l++){
            for(int i=0;i+l<n;i++){
                int j = i+l;
                if(l==0){
                     da[i][j] = 1;
                }
                else if(l==1){
                     da[i][j] = (s[i] == s[j]);
                }
                else{
                    da[i][j] = (da[i+1][j-1] && s[i]==s[j]);
                }
                if(da[i][j] && ans.size()<l+1){
                    ans = s.substr(i,l+1);
                }
            }
        } 
        return ans;
    }
};
