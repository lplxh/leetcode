class Solution {
public:
    int searchleftrange(vector<int>& nums,int target){
        int num=nums.size();
        if(num==0) return -1;
        int left=0,right=num-1;
        while(left<right){
            int mid=left+(right-left)/2;
            if(nums[mid]>target) right=mid-1;
            else if(nums[mid]<target) left=mid+1;
            else right=mid;
        }
        if(nums[left]!=target) return -1;
        else return left; 
    }
    int searchrightrange(vector<int>& nums,int target){
        int num=nums.size();
        if(num==0) return -1;
        int left=0,right=num-1;
        while(left<right){
            int mid=left+(right-left+1)/2;
            if(nums[mid]>target) right=mid-1;
            else if(nums[mid]<target) left=mid+1;
            else left=mid;
        }
        if(nums[right]!=target) return -1;
        else return right; 
    }
    vector<int> searchRange(vector<int>& nums, int target) {
        int left = searchleftrange(nums,target);
        if(left==-1) return {-1,-1};
        int right = searchrightrange(nums,target);
        return {left,right};    
    }
};
