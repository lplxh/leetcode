class Solution {
public:
    string convert(string s, int numRows) {
        if (numRows == 1)  return s;
        vector<string> buf(min(numRows,int(s.length())));
        bool direction = false; 
        int index = 0;
        for(char c : s){
             buf[index] += c;
             if(index == 0 || index == numRows-1){
                  direction = !direction;
             } 
             index += (direction ? 1:-1);
        }
        string ret;
        for(string row : buf){
            ret+=row;
        }
        return ret;
    }
};
