class Solution {
public:
    vector<int> findDiagonalOrder(vector<vector<int>>& matrix) {
        vector<int> ans;
        if(matrix.empty()) return ans;
        int M=matrix.size();
        int N=matrix[0].size();
        int line=0,row=0;
        while(1){
            while(line>0 && row<N-1){ ans.push_back(matrix[line--][row++]);} 
            ans.push_back(matrix[line][row]);
            if(line==M-1 && row==N-1) break;
            if(row==N-1) {line+=1 ;}
            else if(line ==0) {row+=1;}
            while(line<M-1 && row>0){ans.push_back(matrix[line++][row--]);} 
            ans.push_back(matrix[line][row]);
            if(line==M-1 && row==N-1) break;
            if(line==M-1){row+=1;}
            else if(row==0) {line+=1;}
        }
        return ans;
    }
};
