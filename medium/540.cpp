class Solution {
public:
    int singleNonDuplicate(vector<int>& nums) {
        int num=nums.size();
        int left=0,right=num-1;
        while(left<right){
            int mid=left+(right-left)/2;
            if(mid%2==1)  mid=mid-1;
            if(nums[mid]==nums[mid+1]) left=mid+2;
            else right=mid;
        }
        return nums[right];
    }
};
