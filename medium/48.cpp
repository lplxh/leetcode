class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        if(matrix.empty()) return;
        int num=matrix.size();
        for(int time=0;time<num/2;time++){
            for(int col=time;col<num-1-time;col++){
                int tmp=matrix[time][col];
                matrix[time][col]=matrix[num-1-col][time];
                matrix[num-1-col][time]=matrix[num-1-time][num-1-col];
                matrix[num-1-time][num-1-col]=matrix[col][num-1-time];
                matrix[col][num-1-time]=tmp;
            }
        }
    }
};