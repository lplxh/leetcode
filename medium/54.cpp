class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        vector<int> ans;
        if(matrix.empty())return ans;
        int line = matrix.size();
        int row = matrix[0].size();
        int s=0,x=line-1,z=0,y=row-1;
        while(1){
            for(int i=z;i<=y;i++) ans.push_back(matrix[s][i]);
            s++;
            if(s>x) break;
            for(int j=s;j<=x;j++) ans.push_back(matrix[j][y]);
            y--;
            if(z>y) break;
            for(int i=y;i>=z;i--) ans.push_back(matrix[x][i]);
            x--;
            if(s>x) break;
            for(int j=x;j>=s;j--) ans.push_back(matrix[j][z]);
            z++;
            if(z>y) break;
        }
        return ans;
    }
};