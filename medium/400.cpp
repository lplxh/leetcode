class Solution {
public:
    int findNthDigit(int n) {
        long long bit=1,size=9,ans;
        while(n-bit*size>0){
            n-=bit*size;
            bit++;
            size*=10;
        }
        int num=pow(10,bit-1)+(n-1)/bit;
        int pos=(n-1)%bit;
        for(int i=0;i<bit-pos;i++){
            ans=num%10;
            num/=10;
        }
        return ans;
    }
};