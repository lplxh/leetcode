class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int once = 0, twice = 0;
        for(int i=0;i<nums.size();i++) {
            once = ~twice & (once ^ nums[i]);
            twice = ~once & (twice ^ nums[i]);
        }
        return once;
    }


};
