class Solution {
public:
    string getHint(string secret, string guess) {
        int num = secret.size();
        int ansA=0,ansB=0;
        map<char,int> buf;
        for(int i=0;i<num;i++){
            buf[secret[i]]++;
            if(guess[i]==secret[i]) ansA++;
        }
        for(int j=0;j<guess.size();j++){
            if(buf[guess[j]]>0) {
                ansB++;
                buf[guess[j]]--;
            } 
        }
        ansB=ansB-ansA;
        return to_string(ansA)+"A"+to_string(ansB)+"B";
    }
};