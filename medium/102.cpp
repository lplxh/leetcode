class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> vec;
        dfs(root,vec,0);
        return vec;
    }
    vector<vector<int>> dfs(TreeNode* root,vector<vector<int>>& vec,int level){
        if(root==NULL) return vec;
        if(vec.size()<=level) vec.resize(level+1);
        vec[level].push_back(root->val);
        dfs(root->left,vec,level+1);
        dfs(root->right,vec,level+1);
        return vec;
    }
};