class Solution {
public:
    vector<int> grayCode(int n) {
        int tmp = 1;
        vector<int> ans;
        for(;n >= 0;n--){
            if(ans.size() == 0){
                ans.push_back(0);
            }else{
                for(int i = tmp-1; i >= 0; --i){
                    ans.push_back(ans[i] + tmp);
                }
                tmp*= 2;
            }
        }
        return ans;
    }
};
