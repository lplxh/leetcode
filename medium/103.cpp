class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        vector<vector<int>> vec;
        dfs(root,vec,0);
        return vec;
    }
    vector<vector<int>> dfs(TreeNode* root,vector<vector<int>>& vec,int level){
        if(root==NULL) return vec;
        if(vec.size()<=level) vec.resize(level+1);
        if(level%2==0)
            vec[level].push_back(root->val);
        else vec[level].insert(vec[level].begin(),root->val);
        dfs(root->left,vec,level+1);
        dfs(root->right,vec,level+1);
        return vec;
    }
};