class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> vec;
        inorder(root,vec);
        return vec;
    }
    vector<int> inorder(TreeNode* root,vector<int>& vec){
        if(root==nullptr) return vec;
        inorder(root->left,vec);
        vec.push_back(root->val);
        inorder(root->right,vec);
        return vec;
    }
};