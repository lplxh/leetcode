class Solution {
public:
    int findMinDifference(vector<string>& timePoints) {
        vector<int> time;
        for(int i=0;i<timePoints.size();i++){
            time.push_back(string2time(timePoints[i]));
        }
        sort(time.begin(),time.end());
        int ans=time[0]+1440-time[time.size()-1];
        for(int i=1;i<timePoints.size();i++){
            ans=min(time[i]-time[i-1],ans);
        }
        return ans;
    }
    int string2time(string s){
        int hour=stoi(s.substr(0,2));
        int minute=stoi(s.substr(3,2));
        return hour*60+minute;
    }
};