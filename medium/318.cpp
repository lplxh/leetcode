class Solution {
public:
    int maxProduct(vector<string>& words) {
        unordered_map<int, int>hash;
        int ans = 0;
        for (auto& word : words){
            int mask = 0, size = word.size();
            for (int i = 0; i < size; i++){
                mask |= 1 << (word[i] - 'a');
            }
            hash[mask] = max(hash[mask], size);
            for (auto& [k, v] : hash){
                if (!(mask & k)){
                    ans = max(ans, size * v);
                }
            }
        }
        return ans;
    }
};