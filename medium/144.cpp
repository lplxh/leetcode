class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
        vector<int> buf;
        preorder(root,buf);
        return buf;
    }
    void preorder(TreeNode* root,vector<int>&buf){
        if(root==nullptr) return;
        buf.push_back(root->val);
        preorder(root->left,buf);
        preorder(root->right,buf);
    }
};