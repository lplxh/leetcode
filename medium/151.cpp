class Solution {
public:
    string reverseWords(string s) {
        string str=removeExtraSpaces(s);
        reverse(str.begin(),str.end());
        int num=str.size();
        int start=0,end=0;
        for(int i=0;i<=num;i++){
            if(str[i]==' '||str[i]=='\0'){
                end=i;
                for(int k=start,j=end-1;k<j;k++,j--){
                    swap(str[k],str[j]);
                }
                start=end+1;
            }

        }
        return str;
    }
    string removeExtraSpaces(string s){
        int num=s.size();
        int label=-1;
        string str;
        for(int i=0;i<num;i++){
            if(s[i]==' '){
                if(label==-1) str+=s[i];
                label=1;
            }
            else{
                label=-1;
                str+=s[i];
            }
        }
        if(str[0]==' ') str.erase(0,1);
        if(str[str.size()-1]==' ') str.erase(str.size()-1,1);
        return str; 
    }
};