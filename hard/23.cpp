class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        int num=lists.size();
        ListNode*head=nullptr;
        for(int i=0;i<num;i++){
            head=merge(head,lists[i]);
        }
        return head;
    }
    ListNode* merge(ListNode*L1,ListNode*L2){
        if(L1==nullptr) return L2;
        if(L2==nullptr) return L1;
        if(L1->val<=L2->val){
            L1->next=merge(L1->next,L2);
            return L1;
        }
        else {
            L2->next=merge(L1,L2->next);
            return L2;
        }
    }
};