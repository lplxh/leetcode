class Solution {
public:
    int maxsum=INT_MIN;
    int maxpass(TreeNode* node){
        if(node==nullptr) return 0;
        int leftsum=max(maxpass(node->left),0);
        int rightsum=max(maxpass(node->right),0);
        maxsum=max(maxsum,leftsum+rightsum+node->val);
        return max(leftsum+node->val,rightsum+node->val);
    }
    int maxPathSum(TreeNode* root) {
        maxpass(root);
        return maxsum;
    }
};