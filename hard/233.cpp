class Solution {
public:
    int countDigitOne(int n) {
        if(n<0) return 0;
        int cur,left,right=0,index=0,ans=0;
        while(n!=0){
            cur=n%10;
            left=n/10;
            if(cur==0) ans+=left*pow(10,index);
            else if(cur==1) ans+=left*pow(10,index)+right+1;
            else ans+=(left+1)*pow(10,index);
            right+=cur*pow(10,index);
            index++;
            n/=10;
        }
        return ans;
    }
};