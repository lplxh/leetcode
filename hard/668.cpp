class Solution {
public:
    int findKthNumber(int m, int n, int k) {
        int left = 1, right = m * n;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (kThCount(m, n, mid) >= k) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }

    int kThCount(int m, int n, int mid) {
        int result = 0, row = 1, col = m;
        while (col > 0 && row <= n) {
            if (col * row <= mid) {
                result += col;
                row ++;
            } else {
                col --;
            }
        }
        return result;
    }
};